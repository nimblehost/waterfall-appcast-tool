<?php

// This script based on the Stacks Sparkle Appcast Tool created by Joe Workman
// https://github.com/joeworkman/stacks-sparkle

// Set these lines to FALSE if you do not want to log update attempts.
// log_updates => logs all good update attempts from Waterfall.
// log_hacks => logs all attempts that are made outside of Waterfall (potential pirates)
$log_updates = TRUE;
$log_hacks = TRUE;
$log_debug = FALSE;

function log_line($text) {
	// You can change the logfile name here
	$logfile = 'appcast-'. date("Y-m") .'.log';
	$log_line = date("D M j Y G:i:s T").' '.$text."\n";
 	$fh = fopen($logfile, 'a');
 	fwrite($fh, $log_line);
 	fclose($fh);
}
function log_connection($prepend) {
	log_line($prepend.' '. $_SERVER['REMOTE_ADDR'] .' USER_AGENT:'. $_SERVER['HTTP_USER_AGENT']);
}

// Test to see if this request is coming from Sparkle
if ( preg_match("/waterfall/i", $_SERVER['HTTP_USER_AGENT']) ) {

	// Specify the base url common to all the appcast files
	// This can be AWS, Dropbox, or your own hosting account
	$base_url = 'https://www.yourdomain.com/updates/';

	// This line parses out the name of the product supplied by Waterfall
	// Example: Mirage/3.1.2 Waterfall/1.0
	preg_match('/^([a-zA-Z0-9_\s-\.&]*)\/\d/', $_SERVER['HTTP_USER_AGENT'], $matches);
	$product_name = $matches[1];

	// The product name will be the actual name of the theme as specified
	// in the RWThemeName key of the Theme.plist file
	// This URL pattern assumes your product names use single words, 
	// with no spaces or version numbers, i.e. Mirage, Phelix, etc.
	$appcast_url = $base_url .'/'.$product_name.'/appcast.xml';

	// The switch statement below will match the product name in the user agent
	// and return the relevant appcast url. Highy recommended that you use this
	// approach if your theme names include spaces. 
	switch ($product_name) {
	    case "Product Name":
	        $appcast_url = $base_url .'/ProductName/appcast.xml';
	        break;
	    default:
			$appcast_url = $base_url .'/'.$product_name.'/appcast.xml';
			break;	
	}

	// Redirect to the appcast url
	header("Location: ".$appcast_url);

	if ($log_updates == TRUE){
		log_connection('UPDATE');
	}
	if ($log_debug == TRUE){
		log_line('*DEBUG PRODUCT_NAME: '. $product_name .' APPCAST_URL: '. $appcast_url);
	}
} else {
	// You can customize the HTML output here...
    echo "<h1>No access to theme updates here.</h1>";
	if ($log_hacks == TRUE){
	    echo "<h3>For security, your IP has been logged... ".$_SERVER['REMOTE_ADDR']."</h3>";
		log_connection('PIRATE');
	}
}
?>