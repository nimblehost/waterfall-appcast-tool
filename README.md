# README #

This appcast tool is for use with [Waterfall](https://www.nimblehost.com/store/waterfall), an automatic theme updating tool for [RapidWeaver](http://realmacsoftware.com). Waterfall uses conventions made popular by the Sparkle framework.

### Why use this tool? It provides more security, and reduces the risk of piracy.###

The "default" approach to providing theme updates is to specify an `SUFeedURL` in the `Info.plist` file, which points to an `appcast.xml` file on the web. 

When pointing directly to such an xml file, unscrupulous tech-savvy users who are willing to make the effort of digging into the core theme files can possibly locate the URL to that appcast file, and load it in a web browser or other feed reader. 

This means there is the potential for the update file to be downloaded directly, even by people who haven't purchased your products. Although this isn't an issue with compiled software, which can limit the application's use, theme files are not compiled software and cannot be limited in such a fashion at this point in time.

This tool helps prevent this problem by only displaying relevant information to the Waterfall plugin itself.

### How to use this tool ###

1. Download the `appcast.php` file.
2. Edit the file to suit your situation. At the very least you will need to change the `$base_url` and the `switch` statement section, to reflect your own product names and locations of their relevant appcast files.
3. When finished editing, upload the `appcast.php` to your web server, AWS, Dropbox, etc.
4. In your theme's `Info.plist` file, edit the `SUFeedURL` to point to the `appcast.php` file, instead of directly to an xml file.

Once these steps are completed, when Waterfall contacts the `SUFeedURL` the `appcast.php` file will verify that it is Waterfall accessing update information, and will redirect Waterfall to the relevant `appcast.xml` for the product specified.